<!DOCTYPE html>
<html lang="vi">

<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="style.css">
    <title>Register</title>
</head>

<body>
    <main>
        <form action="register.php" method="post">
            <div class="container">
                <div>
                    <label for="name">Họ và tên</label>
                    <input type="text" class="input" name="name">
                </div>

                <div>
                    <label for="gender">Giới tính</label>
                    <?php
                    $gender = array("Nam", "Nữ");
                    for ($i = 0; $i < count($gender); $i++) {
                        echo "<input type='radio' id=$i name='gender' value=$gender[$i]>$gender[$i] ";
                    }
                    ?>
                </div>

                <div>
                    <label for="faculty">Phân khoa</label>
                    <select>
                        <?php
                        $faculties = array(
                            "None" => "",
                            "MAT" => "Khoa học máy tính",
                            "KDL" => "Khoa học vật liệu",
                        );
                        foreach ($faculties as $key => $value) {
                            echo "<option value=$key>$value</option>";
                        }
                        ?>
                    </select>
                </div>

                <button type="submit" class="button" name="register">Đăng ký</button>
        </form>
    </main>
</body>

</html>